const mongoose = require("mongoose");

const cookingCourseSchema = new mongoose.Schema(
  {
    title: String,
    description: String,
    maxStudents: {
      type: Number,
      default: 0,
      min: [0, "Ce cours ne peut pas avoir un nombre négatif d'étudiants"],
    },
    price: {
      type: Number,
      default: 0,
      min: [0, "Le prix ne peut être négatif"],
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("course", cookingCourseSchema);

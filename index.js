const mongoose = require("mongoose");
const express = require("express");
const cookingRouter = require("./routes/cookingRouter");

main().catch((err) => console.log(err));

async function main() {
  await mongoose.connect("mongodb://127.0.0.1:27017/cooking");
  console.log("db connect");
}

const app = express();

const port = process.env.PORT || 5000;

app.use(express.urlencoded({ entended: false }));
app.use("/courses", cookingRouter);

app.listen(port, () => {
  console.log(`Server is running on port: ${port}`);
});

initialisation
$ npm init -y

install express
$ npm i express

install mongoose
$ npm i mongoose

Create index.js

Create models/cookingCourse

Install Nodemon
$ npm i nodemon

Modification dans package.json
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
 "scripts": {
    "dev": "nodemon"
  },

Create routes/cookingRouter.js

Lancement du serveu
$ npm run dev

Ouvrir Postman
http://localhost:5000/courses/new => x-www-form-urlencoded
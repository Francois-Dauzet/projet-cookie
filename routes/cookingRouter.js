const Course = require("../models/cookingCourse");

const cookingRouter = require("express").Router();

//* create
cookingRouter.post("/new", async (req, res) => {
  try {
    let newCourse = new Course(req.body);
    await newCourse.save();

    res.send({
      msg: "Le nouveau cours a été ajouté",
      newCourse,
    });
  } catch (error) {
    console.error(error);
  }
});

//* read all
cookingRouter.post("/all", async (req, res) => {
  try {
    let courses = await Course.find();
    res.send(courses);
  } catch (error) {
    console.error(error);
  }
});

//* read
cookingRouter.get("/:id", async (req, res) => {
  try {
    let course = await Course.findOne({ _id: req.params.id });
    res.send(course);
  } catch (error) {
    console.error(error);
  }
});

//* update
cookingRouter.put("/:id", async (req, res) => {
  try {
    let course = await Course.findOneAndUpdate(
      { _id: req.params.id },
      req.body
    );

    res.send({ msg: "Le cours a été mis à jour", course });
  } catch (error) {
    console.error(error);
  }
});

//* delete
cookingRouter.delete("/:id", async (req, res) => {
  try {
    await Course.findOneAndRemove({ _id: req.params.id });

    res.send("Le cours a bien été supprimé");
  } catch (error) {
    console.error(error);
  }
});

module.exports = cookingRouter;
